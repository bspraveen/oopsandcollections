package list;

import java.util.LinkedList;

public class DemoLinkedlist {

	int id;
	String name;
	public void Student(int id,String name)
	{
		this.id=id;
		this.name=name;
		
	}
	
	public static void main(String[] args) {
		
		DemoLinkedlist S1= new DemoLinkedlist();
		DemoLinkedlist S2= new DemoLinkedlist();
		DemoLinkedlist S3= new DemoLinkedlist();
		DemoLinkedlist S4= new DemoLinkedlist();
		
		S1.Student(1, "Praveen");
		S2.Student(2,"Sarath");
		S3.Student(3, "Rahul");
		S4.Student(4, "Parth");
		 
		LinkedList<DemoLinkedlist> L= new LinkedList<DemoLinkedlist>();
		L.add(S1);
		L.add(S2);
		L.add(S3);
		L.add(0, S4);
		L.add(S1);
		
		System.out.println(L);//we get the hashcode of the elements we stored
		
		LinkedList<String> ll    = new LinkedList<String>();

    
	    ll.add("A");
	    ll.add("B");
	    ll.addLast("C");
	    ll.addFirst("D");
	    ll.add(2, "E");
	    ll.add(0, "Y");//add() adds new element at a desired location
	    ll.set(1, "L");// set() for update of certain element with desired element at a location
	
	    System.out.println(ll);
	    ll.sort(null);
	    System.out.println(ll);
	    ll.remove("B");
	    ll.remove(3);
	   
	    ll.removeFirst();
	    
	    ll.removeLast();
	    
	    System.out.println(ll);
	}
		

	}


