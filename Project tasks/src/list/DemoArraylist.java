package list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoArraylist {
	public void doArrayListExample() {
		ArrayList<String> listA=new ArrayList<String>();
		listA.add("This");
		listA.add("is");
		listA.add("my");
		listA.add("Program");
		listA.add("on");
		listA.add("ArrayList");
		System.out.println("Objects in an ArrayList using index.\n");
		for(int j=0;j<listA.size();j++) 
		{
		System.out.println("["+j+"]-" + listA.get(j));
		}
		int locationIndex=listA.indexOf("This");
		
		System.out.println("location of\"This\"is:"+locationIndex);
		System.out.println("LastIndex of\"This\"" +listA.lastIndexOf("This"));
		
		List<String> listSub=listA.subList(2,listA.size());
		System.out.println("New sub-List from index 2 to "+listA.size());
		System.out.println("\nOriginal sub List :"+listSub);
		Collections.sort(listSub);
		
		System.out.println("Sorted sub list:"+listSub);
		Collections.reverse(listSub);
		System.out.println("\nReversed subList :"+listSub);
		
		System.out.println("\nIs List A empty? "+ listA.isEmpty());
		
		System.out.println("\n\"ArrayListis Contained ?="+listA.contains("ArrayList"));
		
		System.out.println("\n Remove \"my\"from ListA.");
		listA.remove("my");
		
		System.out.println("Remove 3 position Element from ListA.");
		listA.remove(3);
		for(int i=0;i<listA.size();i++)
			System.out.println(listA.get(i));
		System.out.println(listA.size());
		System.out.println("update 4th position Element in listA.");
		listA.set(3,"MA RAJU");
		for(int i=0;i<listA.size();i++)
			System.out.println(listA.get(i));
		
		System.out.println("List A (before)Remove All:"+listA);
		listA.clear();
		
		System.out.println("List A (after) Remove All:"+listA);
		}
	
	
	
	
	
		

	public static void main(String[] args) {
		ArrayList<Object> al =new ArrayList<Object>();
		al.add(1);
		al.add("MSD");
		al.add('s');
		al.add(0, 5);
		System.out.println(al);
		
		DemoArraylist ABC =new DemoArraylist();
		ABC.doArrayListExample();
		// TODO Auto-generated method stub

	}

}
