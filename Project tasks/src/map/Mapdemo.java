package map;

import java.util.*;
import java.util.Map.Entry;

public class Mapdemo {

	public static void main(String[] args) {
		Map<Integer, String> supercars= new HashMap<Integer, String>();
		supercars.put(1, "Ferrari");
		supercars.put(2, "Lamborgini");
		supercars.put(3, "Bugatti");
		supercars.putIfAbsent(4, "Honda");
		supercars.putIfAbsent(1, "Suzuki");
		
		Set<Entry<Integer, String>> set = supercars.entrySet();
		Iterator<Entry<Integer, String>> i = set.iterator();
		while(i.hasNext())
		{
			Map.Entry entry =(Map.Entry)i.next();
			
			System.out.println(entry.getKey()+ "  "+ entry.getValue());
		}
		Map<Integer, String> cars=new HashMap<Integer, String>();
		cars.put(1, "Mahindra");
		cars.put(2, "Chevrolet");
		
		cars.putAll(cars);
		Set<Entry<Integer, String>> set1= cars.entrySet();
		Iterator<Entry<Integer, String>> it = set1.iterator();
		while(it.hasNext()) {
			Map.Entry entry1=(Map.Entry)it.next();
			System.out.println(entry1.getKey()+ "  "+ entry1.getValue());
		}
		
		
	}

}
