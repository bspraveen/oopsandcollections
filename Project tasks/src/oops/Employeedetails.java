package oops;
//Aggregation Example
class Address {

	String city, state, country;
	
	public Address (String city, String state, String country) {
		this.city=city;
		this.state=state;
		this.country=country;
	}
}
public class Employeedetails {

	int id;
	String Name;
	Address address;
	
	public  Employeedetails(int id, String Name, Address address) {
		this.id=id;
		this.Name=Name;
		this.address=address;
	}
	private void display() {
		System.out.println(id +"  "+ Name);
		System.out.println(address.city +"  "+ address.state+ "  "+ address.country);
		
	}
	public static void main(String[] args) {
		
		Address address1=new Address("VSP","AP","India");
		Employeedetails employee=new Employeedetails(1, "Praveen", address1);
		
		employee.display();
		
		
		
	}

}
