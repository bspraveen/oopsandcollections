package Java8;

import java.util.Arrays;
import java.util.List;

public class Foreach {

	public static void main(String[] args) 
		{
			List<Integer> values = Arrays.asList(1,2,3,4,5,6);
		//	for(int i=0;i<values.size();i++)
		//		{
		//			System.out.println(values.get(i));
		//		}
	
	//enhanced forloop
//			for (int i:values)
//		{
//			System.out.println(i);
//		}
	
			//forEach is internal part of collection hence it is faster in execution
			values.forEach(i -> System.out.println(i));
			//
		
		}
}
