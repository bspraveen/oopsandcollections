/**
 * 
 */
package Java8;

import java.util.Arrays;
import java.util.List;

/**
 * @author sai
 *
 */
public class Method_ref {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List <String> names=Arrays.asList("Roy","Tagore","sagar","vivek");
	//	names.forEach(str -> System.out.println(str));
	names.forEach(System.out::println);
	
	}

}
