package Java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Consumer_interf  implements Consumer<Integer>{

	public void accept(Integer i)
	{
		System.out.println(i);
	}
		
	public static void main(String[] args) {
		List<Integer> values = Arrays.asList(11,12,23,34,55);
		
	//	Consumer<Integer> con= new Consumer_interf();
	//	values.forEach(con);

	//	Consumer <Integer> c=//  new Consumer<Integer>() {
	//		public void accept //		(Integer i) {
	//			System.out.println(i); 	}
		
	//	Consumer <Integer > c= i -> System.out.println(i);
		//values.forEach(c);
	 values.forEach(i -> System.out.println(i));
		
	}
}		
	
