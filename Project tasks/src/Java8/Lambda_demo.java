package Java8;
interface inter
{
	void show( int i);
}
//class Xyz implements inter
//{
//	public void show(int i) 
//	{
//		System.out.println("hi");
//	}
//}
public class Lambda_demo {

	public static void main(String[] args) {
//		inter obj;
//		obj = new Xyz();
//		obj.show();
	
		inter obj;
//		obj =new inter()
				{	//this is a class now . a block of code which has methods in it is a class.  This here is an "anonymous inner class"
//			public void show (int i)		//for this method,, parameter, return type, method name are already mentioned in interface. hence not reqd.
//			{									
//				System.out.println("Hello");
//			}

//	obj =()->{
//		System.out.println("hello");//since this is only on line we can remove braces.
//	};
//	obj.show();
					obj=(int i)->System.out.println("Hello" + i);
					obj.show(5);
				}
	}
}
				
		 
		




